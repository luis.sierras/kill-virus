package luis;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe gràfica de tipus finestra (JFrame) on es situa un panell
 * on es mouen i reboten pilotes que es creen amb un botó (JButton)
 * que està situat al sud de la finestra.
 * Utilitzo una classe auxiliar PanelVirus que és on es creen i es mouen les pilotes.
 *
 * @author Montse
 * @version 14/10/2021
 */
public class FinestraJoc extends JFrame {

    private final PanelVirus panelPilotes = new PanelVirus();

    public FinestraJoc() {
        setBounds(600, 300, 400, 350);
        setTitle("Viruses corren");
        add(panelPilotes, BorderLayout.CENTER);
        JPanel botonera = new JPanel();
        JButton boto1 = new JButton("CocBig");
        JButton boto2 = new JButton("Alcohol");
        botonera.add(boto1);
        botonera.add(boto2);
        boto1.addActionListener(new ClickBotoPilotaVa());
        boto2.addActionListener(new ClickSortir());
        add(botonera, BorderLayout.SOUTH);
    }

    static class ClickSortir implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            System.exit(0);
        }
    }

    class ClickBotoPilotaVa implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            for (int i = 0; i < 5; i++) {
                panelPilotes.addVirus();
            }
        }
    }
}
