package luis;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * El panell s'encarrega de crear les pilotes, afegir-les a una llista i pintar-les
 * al mètode paint()
 *
 * @author Montse
 * @version 14/10/2021
 */
class PanelVirus extends JPanel implements MouseListener {
    public PanelVirus() {
        addMouseListener(this);
    }

    private final ArrayList<Virus> viruses = new ArrayList<>();

    public void addVirus() {
        //es crea una pilota, ara és un thread
        Virus v = new Virus();
        Thread virus = new Thread(v);
        //se li passa el panell, per saber les dimensions on ha de rebotar
        v.setElPanelComu(this);
        //s'afegeix a una llista de pilotes, per quan hi hagi vàries
        viruses.add(v);
        //es posa en marxa el thread
        virus.start();
    }

    /**
     * Mètode que no es pot canviar de nom. Quan es vol pintar un objecte gràfic sobre un Component
     * de Swing de Java.
     *
     * @param g de tipus Graphics
     */
    public void paint(Graphics g) {
        super.paint(g);
        /* itera i dibuixa totes les pilotes*/
        for (Virus v : viruses) {
            v.pintarVirus(g);
        }
        //Tip per assegurar que el Graphics s'actualitza
        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int xMouse = e.getX();
        int yMouse = e.getY();
        for (Virus virus:viruses) {
            if (!virus.isDead()){
                virus.matar(xMouse,yMouse);
            }
        }
        System.out.println("(" + xMouse + "," + yMouse + ")");
    }


    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
