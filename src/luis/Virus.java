package luis;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Classe bàsica Virus. Versió Thread per evitar esperes actives de les GUIs
 * Una pilota que es mou i rebota dintre d'uns límits
 * En aquest exemple, els límits venen donats per un panell (JPanel) contenidor
 *
 * @author Montse
 * @version 14/10/2021
 */
public class Virus implements Runnable {
    private int x = (int) ((Math.random()*200)+1);
    private int  y = (int) ((Math.random()*200)+1);
    private double dx = (Math.random()*3)+1;
    private double dy = (Math.random()*3)+1;
    private final int radi = 30;
    private boolean isDead = false;
    Image virus =new ImageIcon(getClass().getClassLoader().getResource("virus.png")).getImage();
    Image hidro =new ImageIcon(getClass().getClassLoader().getResource("hidro.png")).getImage();

    private JPanel elPanelComu; //el panell on reboten les pilotes

    public void setElPanelComu(JPanel p) {
        elPanelComu = p;
    }

    /* es desplaça la pilota 1 posició en x i en y*/
    /* quan arriba als limits, canvia de direcció */

    public void moureVirus() {
        Rectangle2D limits = elPanelComu.getBounds();
        double width = limits.getWidth();
        double height = limits.getHeight();
        x += dx;
        y += dy;
        if (x + radi / 2 > width || x + radi / 2 < 0) dx = -dx;
        if (y + radi / 2 > height || y + radi / 2 < 0) dy = -dy;
    }

    public boolean pintarVirus(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        if (isDead){
            return g2.drawImage(hidro,x, y, radi, radi, null);
        }
        return g2.drawImage(virus,x, y, radi, radi, null);
    }

    @Override
    public void run() {
        while(!isDead) {
            moureVirus();
            try {
                Thread.sleep(10);
            } catch (Exception ignored) {}
            elPanelComu.repaint();
        }
        elPanelComu.repaint();
    }

    public void matar(int xMouse, int yMouse) {
        for (int i=x; i <= x+radi; i++){
            for (int j = y; j <= y+radi ; j++) {
                if (xMouse==i && yMouse==j){
                    isDead=true;
                }else if (xMouse==-i && yMouse==j){
                    isDead=true;
                }else if (xMouse==i && yMouse==-j){
                    isDead=true;
                }else if (xMouse==-i && yMouse==-j){
                    isDead=true;
                }
            }
        }
    }

    public boolean isDead() {
        return isDead;
    }
}
